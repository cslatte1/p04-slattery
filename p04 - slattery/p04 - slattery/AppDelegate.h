//
//  AppDelegate.h
//  p04 - slattery
//
//  Created by Ciaran Slattery on 3/8/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

